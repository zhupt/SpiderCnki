package com.lius.spider.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

import com.lius.spider.common.bean.HospitalBean;
import com.lius.spider.dao.DoctorDao;
import com.lius.spider.dao.impl.DoctorDaoImpl;

public class SpringTest {
	public static void main(String[] args) {
		ApplicationContext app = new FileSystemXmlApplicationContext("target/classes/applicationContext.xml");
		JdbcTemplate template = (JdbcTemplate) app.getBean("jdbcTemplate");
		DoctorDao doctor = new DoctorDaoImpl(template);
		HospitalBean hospital = new HospitalBean();
		hospital.setName("����ҽԺ");
		System.out.println(doctor.queryHospitalId(hospital));
	}
}
