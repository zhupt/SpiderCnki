package com.lius.spider.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.io.IOUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

import com.lius.spider.common.bean.PageParam;
import com.lius.spider.common.constants.PageType;
import com.lius.spider.process.AreaPageProcessor;

public class Start {
	public static void main(String[] args) {
		try {
//			startAreaThread(args[0], args[1], args[2]);
			startAreaThread("target/classes/applicationContext.xml", "D:/00.workspace/90.code/90.java/spider/spider.conf", "C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Command is error.");
			System.out.println("Example:java -jar *.jar [springConf] [confFilePath] [firefox.exe path]");
		}
	}
	
	public static void startAreaThread(String springConf, String confPath, String firefox) throws InterruptedException {
		// "target/classes/applicationContext.xml"
		ApplicationContext app = new FileSystemXmlApplicationContext(springConf);
		JdbcTemplate template = (JdbcTemplate) app.getBean("jdbcTemplate");
		
		LinkedBlockingQueue<PageParam> paramQueue = new LinkedBlockingQueue<PageParam>(10);
		
		AreaPageProcessor areaProcessor = new AreaPageProcessor(template, paramQueue, firefox);
		Thread areaThread = new Thread(areaProcessor);
		areaThread.start();
		
		File file = new File(confPath);
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
			String line = null;
            
            while ((line = br.readLine()) != null) {
            	String[] paramArr = line.split(";");
            	PageParam param = new PageParam("http://www.haodf.com/yiyuan/" + paramArr[0] + "/list.htm", PageType.AREA, null);
            	param.setProvince(paramArr[1]);
            	paramQueue.put(param);
            }
            PageParam param = new PageParam(null, PageType.FINISH, null);
            paramQueue.put(param);
            System.out.println("finish");
		} catch (Exception e) {
		} finally {
			IOUtils.closeQuietly(br);
		}
	}
}
