package com.lius.spider.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

public class Test3 {
	public static void main(String[] args) throws IOException {
//		File file = new File("D:/spider.conf");
//		
//		BufferedReader br = new BufferedReader(new FileReader(file));
//		String line = null;
//        
//        while ((line = br.readLine()) != null) {
//        	
//        	System.out.println(line);
//        }
//	    br.close();
		File imageFile = new File("D:/checkcode.aspx.gif");
		Tesseract tessreact = new Tesseract();
		tessreact.setDatapath("D:/tessdata");
		try {
			String result = tessreact.doOCR(imageFile);
			System.out.println(result);
		} catch (TesseractException e) {
			System.err.println(e.getMessage());
		}
	}
}
