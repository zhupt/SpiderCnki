package com.lius.spider.test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import com.lius.spider.common.bean.HospitalBean;
import com.lius.spider.common.bean.WorkingTime;
import com.lius.spider.common.constants.WorkingTimeType;

public class Test {

	public static void main(String[] args) {
		File pathToBinary = new File("E:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe");
        FirefoxBinary ffBinary = new FirefoxBinary(pathToBinary);
        FirefoxProfile firefoxProfile = new FirefoxProfile();
        firefoxProfile.setPreference("permissions.default.stylesheet", 2);
        firefoxProfile.setPreference("dom.ipc.plugins.enabled.libflashplayer.so",false);
        FirefoxDriver driver = new FirefoxDriver(ffBinary,firefoxProfile);
        
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        
        String url = "http://www.haodf.com/doctor/DE4r0eJWGqZNKNh0L6g2JGYX-4THvahP.htm";
        driver.get(url);
        System.out.println(driver.getTitle());
        
        List<WebElement> voteElements = null;
		try {
			voteElements = driver.findElement(By.id("bp_doctor_getvote")).findElement(By.id("doctorgood")).findElement(By.className("ltdiv")).findElement(By.tagName("table")).findElements(By.tagName("a"));
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
        
        // 医生
//        String name = driver.findElement(By.id("doctor_header")).findElement(By.className("lt")).getText().trim();
//        System.out.println(name);
//        
//        List<WebElement> aboutElements = driver.findElement(By.id("bp_doctor_about")).findElement(By.className("middletr")).findElement(By.tagName("table")).findElements(By.tagName("tr"));
//        for (WebElement e : aboutElements) {
//        	String text = e.getText();
//        	
//        	List<WebElement> tdElements = e.findElements(By.tagName("td"));
//        	System.out.println(tdElements.size());
//        	if (3 == tdElements.size()) {
//        		if (-1 != text.indexOf("职　　称：")) {
//        			System.out.println(text.substring(text.indexOf("：") + 1, text.length()).trim());
//        		}
//        		
//        		if (-1 != text.indexOf("擅　　长：")) {
//        			System.out.println(text.substring(text.indexOf("：") + 1, text.length()).trim());
//        		}
//        		if (-1 != text.indexOf("执业经历：")) {
//        			e.findElements(By.tagName("a")).get(1).click();
//        			System.out.println(e.findElement(By.id("full")).getText().trim());
//        		}
//        		continue;
//        	}
//        	if (4 == tdElements.size()) {
//        		List<WebElement> usefullElements = tdElements.get(tdElements.size() - 1).findElements(By.tagName("a"));
//        		for (WebElement ae : usefullElements) {
//        			text = ae.getText();
//        			if (-1 == text.indexOf("写") && -1 != text.indexOf("感谢信")) {
//        				System.out.println(text.substring(text.indexOf("信") + 1, text.length() - 1).trim());
//        			}
//        			if (-1 == text.indexOf("赠送") && -1 != text.indexOf("礼物")) {
//        				System.out.println(text.substring(text.indexOf("物") + 1, text.length() - 1).trim());
//        			}
//        		}
//        		continue;
//        	}
//        	
//        	if (1 == tdElements.size()) {
//        		System.out.println(e.findElement(By.tagName("img")).getAttribute("src"));
//        	}
//        }
//        
//        WebElement workingTimeTable = driver.findElement(By.id("bp_menzhen")).findElement(By.className("middletr")).findElement(By.className("time_form_tr")).findElement(By.tagName("table"));
//        List<WebElement> trs = workingTimeTable.findElements(By.tagName("tr"));
//        for (int i = 1; i < trs.size(); i++) {
//        	WebElement tr = trs.get(i);
//        	List<WebElement> tds = tr.findElements(By.tagName("td"));
//        	
//        	for (int j = 1; j < tds.size(); j++) {
//        		WebElement td = tds.get(j);
//        		if ("top".equals(td.getAttribute("valign"))) {
//        			WorkingTime workTime = new WorkingTime();
//        			workTime.setType(WorkingTimeType.getByOrdinal(i - 1));
//        			workTime.setWeek(j);
//        			System.out.println(workTime);
//        		}
//        		
//        	}
//        }
        
        // 科室
//        WebElement rootElement = driver.findElement(By.className("cont980"));
//        List<WebElement> departmentParantElements = rootElement.findElements(By.className("cont960"));
//        for (WebElement e : departmentParantElements) {
//        	String text = e.getText();
//        	System.out.println(text);
//        	if (-1 != text.indexOf(">\n首页")) {
//        		text = text.substring(0, text.lastIndexOf(">") - 1);
//        		System.out.println(text.substring(text.lastIndexOf(">") + 1, text.length()).trim());
//        		break;
//        	}
//        }
//        
//        List<WebElement> aboutElements = rootElement.findElement(By.id("about")).findElements(By.tagName("tr"));
//        for (WebElement e : aboutElements) {
//        	String text = e.getText();
//        	if (-1 != text.indexOf("科室电话：")) {
//        		System.out.println(text.substring(text.indexOf("：") + 1, text.length()).trim());
//        		break;
//        	}
//        }
//        
//        int totalPage = 1;
//        try {
//        	List<WebElement> pageElements = rootElement.findElement(By.className("p_bar")).findElements(By.className("p_text"));
//        	for (WebElement e : pageElements) {
//        		String text = e.getText();
//        		if (-1 != text.indexOf("共")) {
//        			totalPage = Integer.valueOf((text.substring(text.indexOf("共") + 1, text.indexOf("页") - 1)).trim());
//        		}
//        	}
//		} catch (Exception e) {
//		}
//        
//        System.out.println(totalPage);
//        
//        for (int i = 1; i <= totalPage; i++) {
//        	url = url.substring(0, url.lastIndexOf(".")) + "/menzhen_" + i + url.substring(url.lastIndexOf("."), url.length());
//        	driver.get(url);
//        	List<WebElement> tableTrs = driver.findElement(By.id("doc_list_index")).findElements(By.tagName("tr"));
//        	for (WebElement e : tableTrs) {
//        		WebElement td = e.findElement(By.tagName("td"));
//        		if ("tdl".equals(td.getAttribute("class"))) {
//        			continue;
//        		}
//        		
//        		System.out.println(td.findElement(By.tagName("a")).getAttribute("href"));
//        	}
//        }
//        科室电话： 	
        
        
        
//        医院
//        WebElement usefullElement = driver.findElement(By.id("contentA"));
//        WebElement hospitalElement = usefullElement.findElement(By.className("panelA_blue"));
//        HospitalBean hospitalInfo = new HospitalBean("null");
//        String hospitalTitle = hospitalElement.findElement(By.className("toptr")).getText();
//        hospitalInfo.setName(hospitalTitle.substring(0, hospitalTitle.indexOf("(")));
//        hospitalInfo.setGrade(hospitalTitle.substring(hospitalTitle.indexOf("(") + 1, hospitalTitle.indexOf(")")));
//        
//        List<WebElement> hospitalDetails = hospitalElement.findElement(By.tagName("table")).findElements(By.tagName("tr"));
//        for (WebElement detail : hospitalDetails) {
//        	String text = detail.getText();
//        	if (-1 != text.indexOf("地　　址：")) {
//        		hospitalInfo.setAddress(text.substring(text.indexOf("：") + 1, text.indexOf("看地图>>") - 1));
//        	}
//        	if (-1 != text.indexOf("电　　话：")) {
//        		hospitalInfo.setTelephone(text.substring(text.indexOf("：") + 1, text.length()));
//        	}
//        }
//        
//        WebElement departmentListElement = usefullElement.findElement(By.className("panelB_blue")).findElement(By.id("hosbra"));
//        List<WebElement> departmentsElement = departmentListElement.findElements(By.tagName("a"));
//        for (WebElement departmentElement : departmentsElement) {
//        	if ("blue".equals(departmentElement.getAttribute("class"))) {
//        		System.out.println(departmentElement.getAttribute("href"));
//        	}
//        }
//        
//        System.out.println(hospitalInfo);
        
//        地区
//        WebElement element = driver.findElement(By.id("el_result_content")).findElement(By.className("bxmd")).findElement(By.className("ct"));
//        List<WebElement> divElements =  element.findElements(By.tagName("div"));
//        for (int i = 0; i < divElements.size(); i++) {
//        	WebElement divElement = divElements.get(i);
//        	if ("m_title_green".equals(divElement.getAttribute("class"))) {
//        		System.out.println("地区：" + divElement.getText() + ", ======" + i);
//        		continue;
//        	} 
//        	if ("m_ctt_green".equals(divElement.getAttribute("class"))) {
//        		List<WebElement> aElements = divElement.findElements(By.tagName("a"));
//        		for (WebElement aElement : aElements) {
//        			System.out.println("医院：" + aElement.getAttribute("href") + ", ======" + i);
//        		}
//        	}
//        }
        
//        for (WebElement aElement : aElements) {
//        	System.out.println(aElement.getAttribute("href"));
//        }

//        driver.get("http://cq.qq.com/baoliao/detail.htm?294064");

//        ArrayList<String> list = new ArrayList<String>();
//        list.add("http://www.haodf.com/doctor/DE4r0BCkuHzduCp0y7VkFGpEHP0h6.htm");
//        list.add("http://www.haodf.com/doctor/DE4r0BCkuHzduCp0yb8hoLSNZn1O5.htm");
//        list.add("http://www.sohu.com");
//        list.add("http://www.163.com");
//        list.add("http://www.qq.com");

//        long start,end;
//
//        for(int i=0;i<list.size();i++){
//            start = System.currentTimeMillis();
//            driver.navigate().to(list.get(i));
//            System.out.println(driver.getTitle());
//            WebElement element = driver.findElement(By.tagName("body"));
//            System.out.println(element.findElement(By.tagName("table")).getText());
//            end = System.currentTimeMillis();
//            System.out.println(list.get(i).toString() + ":" + (end - start));
//            WebElement usefullElement = element.findElement(By.id("bp_doctor_getvote"));
//            WebElement aElement = usefullElement.findElement(By.tagName("a"));
//            aElement.click();
//            System.out.println(driver.getTitle());
//            System.out.println("==================================================================");
//        }

        driver.close();
        driver.quit();
	}
}
