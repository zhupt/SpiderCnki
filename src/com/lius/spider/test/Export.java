package com.lius.spider.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

import com.lius.spider.common.utils.Exporter;

public class Export {

	public static void main(String[] args) {
		ApplicationContext app = new FileSystemXmlApplicationContext("target/classes/applicationContext.xml");
		JdbcTemplate template = (JdbcTemplate) app.getBean("jdbcTemplate");
		
		Exporter exoprter = new Exporter(template);
		exoprter.createExcel();
		System.out.println("over.");
	}
}
