package com.lius.spider.process;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.jdbc.core.JdbcTemplate;

import com.lius.spider.common.bean.DoctorBean;
import com.lius.spider.common.bean.PageParam;
import com.lius.spider.common.bean.WorkingTime;
import com.lius.spider.common.constants.WorkingTimeType;
import com.lius.spider.common.utils.SpiderUtils;

public class DoctorPageProcessor extends AbstractPageProcessor {

	public DoctorPageProcessor(JdbcTemplate jdbcTemplate,
			LinkedBlockingQueue<PageParam> paramQueue, String firefox) {
		super(jdbcTemplate, paramQueue, firefox);
	}

	private static final String BASE_PATH = "/photos";


	@Override
	void transPage(PageParam param) throws Exception {
		String name = driver.findElement(By.id("doctor_header")).findElement(By.className("lt")).findElement(By.className("lt_name")).getText().trim();
//        System.out.println(name);
        DoctorBean doctor = new DoctorBean(param.getParentId(), name, param.getUrl());
        
        buildUpDoctorInfoByAboutInfo(doctor);
        buildUpDoctorByPersionInfo(doctor);
        buildUpDoctorByVotes(doctor);
        
        try {
        	WebElement workingTimeTable = driver.findElement(By.id("bp_menzhen")).findElement(By.className("middletr")).findElement(By.className("time_form_tr")).findElement(By.tagName("table"));
        	List<WorkingTime> workingTimes = new ArrayList<WorkingTime>();
        	List<WebElement> trs = workingTimeTable.findElements(By.tagName("tr"));
        	for (int i = 1; i < trs.size(); i++) {
        		WebElement tr = trs.get(i);
        		List<WebElement> tds = tr.findElements(By.tagName("td"));
        		
        		for (int j = 1; j < tds.size(); j++) {
        			WebElement td = tds.get(j);
        			if ("top".equals(td.getAttribute("valign"))) {
        				WorkingTime workTime = new WorkingTime();
        				workTime.setType(WorkingTimeType.getByOrdinal(i - 1));
        				workTime.setWeek(j);
        				workingTimes.add(workTime);
        			}
        			
        		}
        	}
        	doctor.setWorkingTimes(workingTimes);
		} catch (Exception e) {
		}
        
        doctorDao.insertDoctor(doctor);
//        System.out.println("doctor : " + doctor);
	}
	
	private void buildUpDoctorInfoByAboutInfo(DoctorBean doctor) {
		List<WebElement> aboutElements = driver.findElement(By.id("bp_doctor_about")).findElement(By.className("middletr")).findElement(By.tagName("table")).findElements(By.tagName("tr"));
        for (WebElement e : aboutElements) {
        	String text = e.getText();
        	
        	List<WebElement> tdElements = e.findElements(By.tagName("td"));
//        	System.out.println(tdElements.size());
        	if (3 == tdElements.size()) {
        		if (-1 != text.indexOf("职　　称：")) {
        			List<WebElement> aElements = e.findElements(By.tagName("a"));
        			if (2 != aElements.size()) {
	        			doctor.setTitle(text.substring(text.indexOf("：") + 1, text.length()).trim());
        			} else {
        				aElements.get(1).click();
        				String value = e.findElement(By.id("full_DoctorSpecialize")).getText().trim();
        				doctor.setTitle(value.substring(0, value.lastIndexOf("<< 收起")));
        			}
//        			System.out.println(text.substring(text.indexOf("：") + 1, text.length()).trim());
        		}
        		
        		if (-1 != text.indexOf("擅　　长：")) {
        			doctor.setSkills(text.substring(text.indexOf("：") + 1, text.length()).trim());
//        			System.out.println(text.substring(text.indexOf("：") + 1, text.length()).trim());
        		}
        		if (-1 != text.indexOf("执业经历：")) {
        			List<WebElement> aElements = e.findElements(By.tagName("a"));
        			if (2 != aElements.size()) {
        				doctor.setExperience("暂无");
        			} else {
        				aElements.get(1).click();
        				String value = e.findElement(By.id("full")).getText().trim();
	        			doctor.setExperience(value.substring(0, value.lastIndexOf("<< 收起")));
	//        			System.out.println(e.findElement(By.id("full")).getText().trim());
        			}
        		}
        		continue;
        	}
        	if (4 == tdElements.size()) {
        		List<WebElement> usefullElements = tdElements.get(tdElements.size() - 1).findElements(By.tagName("a"));
        		for (WebElement ae : usefullElements) {
        			text = ae.getText();
        			if (-1 == text.indexOf("写") && -1 != text.indexOf("感谢信")) {
        				doctor.setThanksLetters(Integer.valueOf(text.substring(text.indexOf("信") + 1, text.length() - 1).trim()));
//        				System.out.println(text.substring(text.indexOf("信") + 1, text.length() - 1).trim());
        			}
        			if (-1 == text.indexOf("赠送") && -1 != text.indexOf("礼物")) {
        				doctor.setGift(Integer.valueOf(text.substring(text.indexOf("物") + 1, text.length() - 1).trim()));
//        				System.out.println(text.substring(text.indexOf("物") + 1, text.length() - 1).trim());
        			}
        		}
        		continue;
        	}
        	
        	// 下载照片
        	if (1 == tdElements.size()) {
        		String localPath = BASE_PATH + "/" + doctor.getDepartmentId() + "/" + doctor.getId() + ".jpg";
        		try {
//					SpiderUtils.downloadFile(e.findElement(By.tagName("img")).getAttribute("src"), localPath);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
        		doctor.setPhoto(localPath);
//        		System.out.println(e.findElement(By.tagName("img")).getAttribute("src"));
        	}
        }
	}
	
	private void buildUpDoctorByPersionInfo(DoctorBean doctor) {
		List<WebElement> persionElements = null;
		try {
			persionElements = driver.findElement(By.id("bp_doctor_about")).findElement(By.className("middletr")).findElement(By.className("doct_data_xxzx")).findElements(By.tagName("tr"));
		} catch (Exception e) {
		}
		
		if (null == persionElements || 4 != persionElements.size()) {
			return;
		}
		
		for (int i = 0; i < persionElements.size(); i++) {
			List<WebElement> tds = persionElements.get(i).findElements(By.tagName("td"));
			if (4 != tds.size()) {
				continue;
			}
			
			if (1 == i) {
				doctor.setCanInternet(1);
				List<WebElement> msgs = persionElements.get(i).findElements(By.className("orange"));
				if (0 == msgs.size()) {
					continue;
				}
				doctor.setPatientQuestionsNo(Integer.valueOf(msgs.get(0).getText()));
				if (1 == msgs.size()) {
					continue;
				}
				doctor.setQuestionsReplyNo(Integer.valueOf(msgs.get(1).getText()));
			}
			if (2 == i) {
				doctor.setCanTelephone(1);
			}
			if (3 == i) {
				doctor.setCanAddNum(1);
				try {
					WebElement msg = persionElements.get(i).findElement(By.className("orange"));
					doctor.setAddNumNo(Integer.valueOf(msg.getText()));
				} catch (Exception e) {
				}
			}
		}
	}
	
	private void buildUpDoctorByVotes(DoctorBean doctor) {
		List<WebElement> voteElements = null;
		try {
			voteElements = driver.findElement(By.id("bp_doctor_getvote")).findElement(By.id("doctorgood")).findElement(By.className("ltdiv")).findElement(By.tagName("table")).findElements(By.tagName("a"));
		} catch (Exception e) {
			return;
		}
		Map<String, Integer> votes = new HashMap<String, Integer>();
		int total = 0;
		for (int i = 0; i < voteElements.size() - 1; i += 2) {
			String key = voteElements.get(i).getText();
			String value = voteElements.get(i + 1).getText();
			int count = Integer.valueOf(value.substring(0, value.length() - 1));
			votes.put(key, count);
			
			total += count;
		}
		
		votes.put("看病经验", total);
		doctor.setVotes(SpiderUtils.convMapToStr(votes));
	}

	@Override
	AbstractPageProcessor getChildPageProcer() {
		return null;
	}

}
