package com.lius.spider.process;

import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.jdbc.core.JdbcTemplate;

import com.lius.spider.common.bean.DepartmentBean;
import com.lius.spider.common.bean.HospitalBean;
import com.lius.spider.common.bean.PageParam;
import com.lius.spider.common.constants.PageType;

public class HospitalPageProcessor extends AbstractPageProcessor {

	public HospitalPageProcessor(JdbcTemplate jdbcTemplate,
			LinkedBlockingQueue<PageParam> paramQueue, String firefox) {
		super(jdbcTemplate, paramQueue, firefox);
	}

	@Override
	void transPage(PageParam param) throws Exception {
		WebElement usefullElement = driver.findElement(By.id("contentA"));
        HospitalBean hospital = transHospitalInfo(usefullElement, param);
        
        if (!init) {
        	int count = doctorDao.queryHospitalCount(hospital);
        	if (0 != count) {
        		hospital.setId(doctorDao.queryHospitalId(hospital));
        		init = true;
        	}
        } else {
        	doctorDao.insertHospital(hospital);
        }
        
        // TODO 保存hospital信息
//        System.out.println("hospital : " + hospital);
        
        WebElement departmentListElement = usefullElement.findElement(By.className("panelB_blue")).findElement(By.id("hosbra"));
        List<WebElement> departmentsElement = departmentListElement.findElements(By.tagName("a"));
        
        // -1 初始化；0 正常执行；1 存在之前执行的数据；
        int status = -1;
        for (int i = 0; i < departmentsElement.size(); i++) {
        	WebElement departmentElement = departmentsElement.get(i);
        	
        	if ("blue".equals(departmentElement.getAttribute("class"))) {
        		if (-1 == status) {
        			DepartmentBean department = new DepartmentBean();
        			department.setName(departmentElement.getText().trim());
        			int count = doctorDao.queryDepartmentCount(department);
        			if (0 != count) {
        				status = 1;
        				continue;
        			} else {
        				status = 0;
        			}
        		}
        		
        		if (1 == status) {
        			DepartmentBean department = new DepartmentBean();
        			department.setName(departmentElement.getText().trim());
        			int count = doctorDao.queryDepartmentCount(department);
        			if (0 != count) {
        				status = 1;
        				continue;
        			} else {
        				status = 0;
        				// 确定i值
        				for (int j = i - 1; j >= 0; j--) {
        					departmentElement = departmentsElement.get(j);
        					if ("blue".equals(departmentElement.getAttribute("class"))) {
        						i = j - 1 < -1 ? -1 : j - 1;
        						break;
        					}
        				}
        				continue;
        			}
        		}
        		
        		PageParam childParam = new PageParam(departmentElement.getAttribute("href"), PageType.DEPARTMENT, hospital.getId());
    			resultQueue.put(childParam);
        	}
        }
        
	}
	
	private HospitalBean transHospitalInfo(WebElement usefullElement, PageParam param) {
		WebElement hospitalElement = usefullElement.findElement(By.className("panelA_blue"));
		
        HospitalBean hospitalInfo = new HospitalBean(param.getParentId());
        String hospitalTitle = hospitalElement.findElement(By.className("toptr")).getText();
        if (-1 != hospitalTitle.indexOf("(")) {
        	hospitalInfo.setName(hospitalTitle.substring(0, hospitalTitle.indexOf("(")));
        	hospitalInfo.setGrade(hospitalTitle.substring(hospitalTitle.indexOf("(") + 1, hospitalTitle.indexOf(")")));
        } else {
        	hospitalInfo.setName(hospitalTitle);
        }
        
        List<WebElement> hospitalDetails = hospitalElement.findElement(By.tagName("table")).findElements(By.tagName("tr"));
        for (WebElement detail : hospitalDetails) {
        	String text = detail.getText();
        	if (-1 != text.indexOf("地　　址：")) {
        		if (-1 != text.indexOf("看地图>>")) {
        			hospitalInfo.setAddress(text.substring(text.indexOf("：") + 1, text.indexOf("看地图>>") - 1));
        		} else {
        			hospitalInfo.setAddress(text);
        		}
        	}
        	if (-1 != text.indexOf("电　　话：")) {
        		hospitalInfo.setTelephone(text.substring(text.indexOf("：") + 1, text.length()));
        	}
        }
        return hospitalInfo;
	}

	@Override
	AbstractPageProcessor getChildPageProcer() {
		return new DepartmentPageProcessor(jdbcTemplate, resultQueue, firefox);
	}

}
