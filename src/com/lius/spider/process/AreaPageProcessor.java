package com.lius.spider.process;

import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.jdbc.core.JdbcTemplate;

import com.lius.spider.common.bean.AreaBean;
import com.lius.spider.common.bean.HospitalBean;
import com.lius.spider.common.bean.PageParam;
import com.lius.spider.common.constants.PageType;

public class AreaPageProcessor extends AbstractPageProcessor {


	public AreaPageProcessor(JdbcTemplate jdbcTemplate,
			LinkedBlockingQueue<PageParam> paramQueue, String firefox) {
		super(jdbcTemplate, paramQueue, firefox);
	}

	@Override
	public void transPage(PageParam param) throws Exception {
		WebElement element = driver.findElement(By.id("el_result_content")).findElement(By.className("bxmd")).findElement(By.className("ct"));
        List<WebElement> divElements =  element.findElements(By.tagName("div"));
        
        String province = param.getProvince();
        
        // -1 初始化；0 从上一个开始执行；1 存在之前执行的数据；2 正常执行
        int status = -1;
        for (int i = 0; i + 3 < divElements.size(); i += 3) {
        	AreaBean area = new AreaBean(province, null);
        	WebElement divElement = divElements.get(i);
        	if ("m_title_green".equals(divElement.getAttribute("class"))) {
//        		System.out.println("地区：" + divElement.getText() + ", ======" + i);
        		area.setCity(divElement.getText());
        		
        		if (-1 == status) {
        			int count = doctorDao.queryAreaCount(area);
        			if (0 != count) {
        				status = 1;
        				continue;
        			} else {
        				status = 2;
        			}
        		}
        		
        		if (1 == status) {
        			int count = doctorDao.queryAreaCount(area);
        			if (0 != count) {
        				status = 1;
        				continue;
        			} else {
        				status = 0;
        				i = i - 6 < -3 ? -3 : i - 6;
            			continue;
        			}
        		}
        		
        		if (2 == status) {
        			doctorDao.insertArea(area);
        		}
        		
        		if (0 == status) {
        			area.setId(doctorDao.queryAreaId(area));
        			status = 2;
        		}
//        		System.out.println("area : " + area);
        	}
        	divElement = divElements.get(i + 1);
//        	System.out.println(divElement.getText());
//        	divElement = divElements.get(i + 2);
//        	System.out.println(divElement.getText());
        	if ("m_ctt_green".equals(divElement.getAttribute("class"))) {
        		List<WebElement> aElements = divElement.findElements(By.tagName("a"));
        		
        		// -1 初始化；0 正常执行；1 存在之前执行的数据
    	        int hospitalStatus = -1;
        		for (int j = 0; j < aElements.size(); j++) {
        			WebElement aElement = aElements.get(j);
        			
        			if (-1 == hospitalStatus) {
        				HospitalBean hospital = new HospitalBean();
        				hospital.setName(aElement.getText().trim());
        				int count = doctorDao.queryHospitalCount(hospital);
        				if (0 != count) {
        					hospitalStatus = 1;
        					continue;
        				} else {
        					hospitalStatus = 0;
        				}
        			}
        			
        			if (1 == hospitalStatus) {
        				HospitalBean hospital = new HospitalBean();
        				hospital.setName(aElement.getText().trim());
        				int count = doctorDao.queryHospitalCount(hospital);
        				if (0 != count) {
        					hospitalStatus = 1;
        					continue;
        				} else {
        					hospitalStatus = 0;
        					j = j - 2 < -1 ? -1 : j - 2;
            				continue;
        				}
        			}
        			
        			PageParam childParam = new PageParam(aElement.getAttribute("href"), PageType.HOSPITAL, area.getId());
        			resultQueue.put(childParam);
//        			System.out.println("医院：" + aElement.getAttribute("href") + ", ======" + i);
        		}
        	}
        }
//        SpiderUtils.printList(areas);
//        SpiderUtils.printQueue(resultQueue);
	}

	@Override
	AbstractPageProcessor getChildPageProcer() {
		return new HospitalPageProcessor(jdbcTemplate, resultQueue, firefox);
	}
}
