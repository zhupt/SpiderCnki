package com.lius.spider.process;

import java.io.File;
import java.util.concurrent.LinkedBlockingQueue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.springframework.jdbc.core.JdbcTemplate;

import com.lius.spider.common.bean.PageParam;
import com.lius.spider.common.constants.PageType;
import com.lius.spider.dao.DoctorDao;
import com.lius.spider.dao.impl.DoctorDaoImpl;

public abstract class AbstractPageProcessor implements Runnable {

	protected LinkedBlockingQueue<PageParam> paramQueue = new LinkedBlockingQueue<PageParam>(10);
	protected LinkedBlockingQueue<PageParam> resultQueue = new LinkedBlockingQueue<PageParam>(10);
	protected JdbcTemplate jdbcTemplate;
	protected DoctorDao doctorDao;
	
	protected WebDriver driver;
	protected String firefox;
	protected boolean init = false;
	
	public AbstractPageProcessor(JdbcTemplate jdbcTemplate, LinkedBlockingQueue<PageParam> paramQueue, String firefox) {
		this.jdbcTemplate = jdbcTemplate;
		this.paramQueue = paramQueue;
		this.doctorDao = new DoctorDaoImpl(jdbcTemplate);
		this.firefox = firefox;
		// "E:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe"
		File pathToBinary = new File(firefox);
        FirefoxBinary ffBinary = new FirefoxBinary(pathToBinary);
        FirefoxProfile firefoxProfile = new FirefoxProfile();
        firefoxProfile.setPreference("permissions.default.stylesheet", 2);
        firefoxProfile.setPreference("dom.ipc.plugins.enabled.libflashplayer.so",false);
        driver = new FirefoxDriver(ffBinary, firefoxProfile); 
	}
	
	public void run() {
		AbstractPageProcessor childProcessor = getChildPageProcer();
		if (null != childProcessor) {
			Thread childThread = new Thread(childProcessor);
			childThread.start();
		}
		
		if (childProcessor instanceof DoctorPageProcessor) {
			for (int i = 0; i < 2; i++) {
				childProcessor = getChildPageProcer();
				if (null != childProcessor) {
					Thread childThread = new Thread(childProcessor);
					childThread.start();
				}
			}
		}
		
		while(true) {
			PageParam param = null;
			try {
				param = paramQueue.take();
				if (PageType.FINISH == param.getType()) {
					driver.close();
					driver.quit();
					finishThread();
					paramQueue.put(param);
					return;
				}
				
				driver.get(param.getUrl());
				transPage(param);
			} catch (Exception e) {
				System.out.println(param);
				e.printStackTrace();
			}
		}
	}
	
	abstract void transPage(PageParam param) throws Exception;
	
	abstract AbstractPageProcessor getChildPageProcer();
	
	protected void finishThread() throws Exception {
		PageParam param = new PageParam(null, PageType.FINISH, null);
		resultQueue.put(param);
	}

}
