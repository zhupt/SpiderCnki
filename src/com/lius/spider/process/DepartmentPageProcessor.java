package com.lius.spider.process;

import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.jdbc.core.JdbcTemplate;

import com.lius.spider.common.bean.DepartmentBean;
import com.lius.spider.common.bean.PageParam;
import com.lius.spider.common.constants.PageType;

public class DepartmentPageProcessor extends AbstractPageProcessor {

	public DepartmentPageProcessor(JdbcTemplate jdbcTemplate,
			LinkedBlockingQueue<PageParam> paramQueue, String firefox) {
		super(jdbcTemplate, paramQueue, firefox);
	}

	@Override
	void transPage(PageParam param) throws Exception {
		WebElement rootElement = driver.findElement(By.className("cont980"));
        
		DepartmentBean department = transDepartmentInfo(rootElement, param);
		
		if (!init) {
        	int count = doctorDao.queryDepartmentCount(department);
        	if (0 != count) {
        		String departmentId = doctorDao.queryDepartmentId(department);
        		department.setId(departmentId);
        		doctorDao.deleteDoctorsByDepartmentId(departmentId);
        		init = true;
        	}
        } else {
        	doctorDao.insertDepartment(department);
        }
		
//		System.out.println("department : " + department);
        
        int totalPage = 1;
        try {
        	List<WebElement> pageElements = rootElement.findElement(By.className("p_bar")).findElements(By.className("p_text"));
        	for (WebElement e : pageElements) {
        		String text = e.getText();
        		if (-1 != text.indexOf("共")) {
        			totalPage = Integer.valueOf((text.substring(text.indexOf("共") + 1, text.indexOf("页") - 1)).trim());
        		}
        	}
		} catch (Exception e) {
		}
        
//        System.out.println(totalPage);
        
        String url = param.getUrl();
        for (int i = 1; i <= totalPage; i++) {
        	driver.get(url.substring(0, url.lastIndexOf(".")) + "/menzhen_" + i + url.substring(url.lastIndexOf("."), url.length()));
        	
        	WebDriverWait wait = (new WebDriverWait(driver, 10));
            wait.until(new ExpectedCondition<WebElement>(){
                public WebElement apply(WebDriver d){
                    return d.findElement(By.id("doc_list_index"));
                }
            });
        	
        	List<WebElement> tableTrs = driver.findElement(By.id("doc_list_index")).findElements(By.tagName("tr"));
        	for (WebElement e : tableTrs) {
        		WebElement td = e.findElement(By.tagName("td"));
        		if ("tdl".equals(td.getAttribute("class"))) {
        			continue;
        		}
        		
        		PageParam childParam = new PageParam(td.findElement(By.tagName("a")).getAttribute("href"), PageType.PERSON, department.getId());
    			resultQueue.put(childParam);
//        		System.out.println(td.findElement(By.tagName("a")).getAttribute("href"));
        	}
        }
		
	}
	
	private DepartmentBean transDepartmentInfo(WebElement rootElement, PageParam param) {
		List<WebElement> departmentParantElements = rootElement.findElements(By.className("cont960"));
		
		DepartmentBean department = new DepartmentBean(param.getParentId());
        for (WebElement e : departmentParantElements) {
        	String text = e.getText();
//        	System.out.println(text);
        	if (-1 != text.indexOf(">\n首页")) {
        		text = text.substring(0, text.lastIndexOf(">") - 1);
        		department.setName(text.substring(text.lastIndexOf(">") + 1, text.length()).trim());
        		break;
        	}
        }
        
        List<WebElement> aboutElements = rootElement.findElement(By.id("about")).findElements(By.tagName("tr"));
        for (WebElement e : aboutElements) {
        	String text = e.getText();
        	if (-1 != text.indexOf("科室电话：")) {
        		department.setTelephone(text.substring(text.indexOf("：") + 1, text.length()).trim());
        		break;
        	}
        }
        
        return department;
	}

	@Override
	AbstractPageProcessor getChildPageProcer() {
		return new DoctorPageProcessor(jdbcTemplate, resultQueue, firefox);
	}

}
