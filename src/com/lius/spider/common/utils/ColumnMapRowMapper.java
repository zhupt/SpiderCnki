package com.lius.spider.common.utils;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.JdbcUtils;

/**
 * <p>
 * Title: ColumnMapRowMapper
 * </p>
 * <p>
 * Description: 灏嗘瘡琛屾槧灏勪负Map瀵硅薄鐨凴owMapper瀹炵幇
 * </p>
 * <p>
 * Company: sunyard
 * </p>
 * 
 * @author liuxt
 * @version 1.0
 */

public class ColumnMapRowMapper implements RowMapper<Map<String, Object>>{

	public Map<String, Object> mapRow(ResultSet rs, int arg1)
			throws SQLException {
		ResultSetMetaData rsmd = rs.getMetaData();
		int columnCount = rsmd.getColumnCount();

		Map<String, Object> mapOfColValues = new HashMap<String, Object>();

		for (int i = 1; i <= columnCount; i++) {
			String key = rsmd.getColumnName(i);
			Object obj = JdbcUtils.getResultSetValue(rs, i);
			mapOfColValues.put(key, "null".equals(String.valueOf(obj)) ? "-" : String.valueOf(obj));
		}
		return mapOfColValues;
	}
}

