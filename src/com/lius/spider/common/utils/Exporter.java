package com.lius.spider.common.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.jdbc.core.JdbcTemplate;

import com.lius.spider.dao.DoctorDao;
import com.lius.spider.dao.impl.DoctorDaoImpl;

public class Exporter {
	
	private final String FILE_NAME = "D:/doctor.xlsx";
	
	private final int LIMIT = 100;
	
	private DoctorDao doctorDao;
	
	public Exporter(JdbcTemplate jdbcTemplate) {
		doctorDao = new DoctorDaoImpl(jdbcTemplate);
	}
	
	public void createExcel() {
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(createFile());
			Workbook wb = new XSSFWorkbook();
			Sheet sheet = wb.createSheet();
			Row row = sheet.createRow(0);
			createHeader(row);
			int totalCount = doctorDao.queryAllDoctorsCount();
			for (int i = 0; i < totalCount / LIMIT + 1; i++) {
				int start = i * LIMIT;
				List<Map<String, Object>> datas = doctorDao.queryAllDoctors(start, LIMIT);
				createDataRows(sheet, start + 1, datas);
			}
			
			wb.write(fos);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			IOUtils.closeQuietly(fos);
		}
	}
	
	private void createHeader(Row row) {
		Cell cell = row.createCell(0);
		cell.setCellValue("姓名");
		cell = row.createCell(1);
		cell.setCellValue("省/市");
		cell = row.createCell(2);
		cell.setCellValue("市/区");
		cell = row.createCell(3);
		cell.setCellValue("医院");
		cell = row.createCell(4);
		cell.setCellValue("医院地址");
		cell = row.createCell(5);
		cell.setCellValue("医院电话");
		cell = row.createCell(6);
		cell.setCellValue("科室");
		cell = row.createCell(7);
		cell.setCellValue("科室电话");
		cell = row.createCell(8);
		cell.setCellValue("职称");
		cell = row.createCell(9);
		cell.setCellValue("擅长");
		cell = row.createCell(10);
		cell.setCellValue("执业经历");
		cell = row.createCell(11);
		cell.setCellValue("感谢信（封）");
		cell = row.createCell(12);
		cell.setCellValue("礼物（个）");
		cell = row.createCell(13);
		cell.setCellValue("门诊时间");
		cell = row.createCell(14);
		cell.setCellValue("看病经验");
		cell = row.createCell(15);
		cell.setCellValue("能否电话问诊");
		cell = row.createCell(16);
		cell.setCellValue("能否在线问诊");
		cell = row.createCell(17);
		cell.setCellValue("能否加号");
	}
	
	private void createDataRows(Sheet sheet, int start, List<Map<String, Object>> datas) {
		for (int i = 0; i < datas.size(); i++) {
			Row row = sheet.createRow(start + i);
			createDataRow(row, datas.get(i));
		}
	}
	
	private void createDataRow(Row row, Map<String, Object> data) {
		Cell cell = row.createCell(0);
		cell.setCellValue(String.valueOf(data.get("name")));
		cell = row.createCell(1);
		cell.setCellValue(String.valueOf(data.get("province")));
		cell = row.createCell(2);
		cell.setCellValue(String.valueOf(data.get("city")));
		cell = row.createCell(3);
		cell.setCellValue(String.valueOf(data.get("hospitalName")));
		cell = row.createCell(4);
		cell.setCellValue(String.valueOf(data.get("address")));
		cell = row.createCell(5);
		cell.setCellValue(String.valueOf(data.get("hospitalTel")));
		cell = row.createCell(6);
		cell.setCellValue(String.valueOf(data.get("departmentName")));
		cell = row.createCell(7);
		cell.setCellValue(String.valueOf(data.get("departmentTel")));
		cell = row.createCell(8);
		cell.setCellValue(String.valueOf(data.get("title")));
		cell = row.createCell(9);
		cell.setCellValue(String.valueOf(data.get("skills")));
		cell = row.createCell(10);
		cell.setCellValue(String.valueOf(data.get("experience")));
		cell = row.createCell(11);
		cell.setCellValue(String.valueOf(data.get("thanksLetters")));
		cell = row.createCell(12);
		cell.setCellValue(String.valueOf(data.get("gift")));
		cell = row.createCell(13);
		cell.setCellValue(String.valueOf(data.get("workingTimes")));
		cell = row.createCell(14);
		cell.setCellValue(String.valueOf(data.get("votes")));
		cell = row.createCell(15);
		cell.setCellValue("1".equals(String.valueOf(data.get("canTelephone"))) ? "是" : "否");
		cell = row.createCell(16);
		cell.setCellValue("1".equals(String.valueOf(data.get("canInternet"))) ? "是" : "否");
		cell = row.createCell(17);
		cell.setCellValue("1".equals(String.valueOf(data.get("canAddNum"))) ? "是" : "否");
	}
	
	private File createFile() throws IOException {
		File file = new File(FILE_NAME);
		if (file.exists()) {
			file.delete();
		}
		file.createNewFile();
		return file;
	}
}
