package com.lius.spider.common.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.io.IOUtils;

import com.lius.spider.common.bean.WorkingTime;
import com.lius.spider.common.constants.WorkingTimeType;

public class SpiderUtils {
	
	public static void printList(List<?> list) {
		for (int i = 0; i < list.size(); i++) {
			System.out.println(i + ":" + list.get(i));
		}
	}
	
	public static void printQueue(LinkedBlockingQueue<?> queue) {
		Object[] array = queue.toArray();
		for (int i = 0; i < array.length; i++) {
			System.out.println(i + ":" + array[i]);
		}
	}
	
	public static void downloadFile(String remoteFilePath, String localFilePath) throws Exception {
		File localFile = new File(localFilePath);
		InputStream is = null;
		FileOutputStream fos = null;
		try {
			URL url = new URL(remoteFilePath);
			URLConnection conn = url.openConnection();  
            is = conn.getInputStream();  
			fos = new FileOutputStream(localFile);
			IOUtils.copyLarge(is, fos);
		} finally {
			IOUtils.closeQuietly(fos);
			IOUtils.closeQuietly(is);
		}
	}
	
	public static String convListToStr(List<WorkingTime> workingTimes) {
		String str = "";
		if (null == workingTimes) {
			return str;
		}
		for (WorkingTime workingTime : workingTimes) {
			str += "��" + workingTime.getWeek() + WorkingTimeType.transEnum(workingTime.getType()) + ";";
		}
		
		return str;
	}
	
	public static String convMapToStr(Map<String, Integer> map) {
		String str = "";
		if (null == map) {
			return str;
		}
		
		for (String key : map.keySet()) {
			str += key + ":" + map.get(key) + ";";
		}
		return str;
	}
}
