package com.lius.spider.common.constants;

public enum PageType {
	AREA,
	HOSPITAL,
	DEPARTMENT,
	PERSON,
	FINISH;
}
