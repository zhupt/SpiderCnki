package com.lius.spider.common.constants;

public enum WorkingTimeType {
	MORNING,
	AFTERNOON,
	NIGHT;
	
	public static WorkingTimeType getByOrdinal(int ordinal) {
		return WorkingTimeType.values()[ordinal];
	}
	
	public static String transEnum(WorkingTimeType type) {
		switch (type) {
		case MORNING:
			return "����";
		case AFTERNOON:
			return "����";
		case NIGHT:
			return "����";
		default:
			return "";
		}
	}
}
