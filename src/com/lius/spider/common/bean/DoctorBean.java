package com.lius.spider.common.bean;

import java.util.List;
import java.util.UUID;

public class DoctorBean {

	private String id;
	
	private String name;
	
	private String photo;
	
	private String departmentId;
	
	// 职称
	private String title;
	
	// 擅长
	private String skills;
	
	// 职业经历
	private String experience;
	
	// 感谢信
	private int thanksLetters;
	
	// 礼物
	private int gift;
	
	private List<WorkingTime> workingTimes;
	
	private String votes;
	
	private int canTelephone;
	
	private int canInternet;
	
	private int canAddNum;
	
	private int patientQuestionsNo;
	
	private int questionsReplyNo;
	
	private int addNumNo;
	
	private String url;
	
	public DoctorBean() {
		
	}
	
	public DoctorBean(String departmentId, String name, String url) {
		this.id = UUID.randomUUID().toString();
		this.departmentId = departmentId;
		this.name = name;
		this.url = url;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSkills() {
		return skills;
	}

	public void setSkills(String skills) {
		this.skills = skills;
	}

	public String getExperience() {
		return experience;
	}

	public void setExperience(String experience) {
		this.experience = experience;
	}

	public int getThanksLetters() {
		return thanksLetters;
	}

	public void setThanksLetters(int thanksLetters) {
		this.thanksLetters = thanksLetters;
	}

	public int getGift() {
		return gift;
	}

	public void setGift(int gift) {
		this.gift = gift;
	}

	public List<WorkingTime> getWorkingTimes() {
		return workingTimes;
	}

	public void setWorkingTimes(List<WorkingTime> workingTimes) {
		this.workingTimes = workingTimes;
	}
	
	public String getVotes() {
		return votes;
	}

	public void setVotes(String votes) {
		this.votes = votes;
	}

	public int getCanTelephone() {
		return canTelephone;
	}

	public void setCanTelephone(int canTelephone) {
		this.canTelephone = canTelephone;
	}

	public int getCanInternet() {
		return canInternet;
	}

	public void setCanInternet(int canInternet) {
		this.canInternet = canInternet;
	}

	public int getCanAddNum() {
		return canAddNum;
	}

	public void setCanAddNum(int canAddNum) {
		this.canAddNum = canAddNum;
	}

	public int getPatientQuestionsNo() {
		return patientQuestionsNo;
	}

	public void setPatientQuestionsNo(int patientQuestionsNo) {
		this.patientQuestionsNo = patientQuestionsNo;
	}

	public int getQuestionsReplyNo() {
		return questionsReplyNo;
	}

	public void setQuestionsReplyNo(int questionsReplyNo) {
		this.questionsReplyNo = questionsReplyNo;
	}

	public int getAddNumNo() {
		return addNumNo;
	}

	public void setAddNumNo(int addNumNo) {
		this.addNumNo = addNumNo;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "DoctorBean [id=" + id + ", name=" + name + ", photo=" + photo
				+ ", departmentId=" + departmentId + ", title=" + title
				+ ", skills=" + skills + ", experience=" + experience
				+ ", thanksLetters=" + thanksLetters + ", gift=" + gift
				+ ", workingTimes=" + workingTimes + ", votes=" + votes
				+ ", canTelephone=" + canTelephone + ", canInternet="
				+ canInternet + ", canAddNum=" + canAddNum
				+ ", patientQuestionsNo=" + patientQuestionsNo
				+ ", questionsReplyNo=" + questionsReplyNo + ", addNumNo="
				+ addNumNo + ", url=" + url + "]";
	}
}
