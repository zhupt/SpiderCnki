package com.lius.spider.common.bean;

import java.util.UUID;

public class HospitalBean {
	
	private String id;
	
	private String areaId;
	
	private String name;
	
	private String grade;
	
	private String address;
	
	private String telephone;
	
	public HospitalBean() {
		
	}
	
	public HospitalBean(String areaId) {
		this.id = UUID.randomUUID().toString();
		this.areaId = areaId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	@Override
	public String toString() {
		return "HospitalBean [id=" + id + ", areaId=" + areaId + ", name="
				+ name + ", grade=" + grade + ", address=" + address
				+ ", telephone=" + telephone + "]";
	}


}
