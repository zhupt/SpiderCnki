package com.lius.spider.common.bean;

import com.lius.spider.common.constants.WorkingTimeType;

public class WorkingTime {
	private WorkingTimeType type;
	
	private int week;

	public WorkingTimeType getType() {
		return type;
	}

	public void setType(WorkingTimeType type) {
		this.type = type;
	}

	public int getWeek() {
		return week;
	}

	public void setWeek(int week) {
		this.week = week;
	}

	@Override
	public String toString() {
		return "WorkingTime [type=" + type + ", week=" + week + "]";
	}
}
