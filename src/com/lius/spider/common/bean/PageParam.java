package com.lius.spider.common.bean;

import com.lius.spider.common.constants.PageType;

public class PageParam {
	
	private String url;
	
	private PageType type;
	
	private String parentId;
	
	private String province;
	
	public PageParam(String url, PageType type, String parentId) {
		this.url = url;
		this.type = type;
		this.parentId = parentId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public PageType getType() {
		return type;
	}

	public void setType(PageType type) {
		this.type = type;
	}
	
	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	@Override
	public String toString() {
		return "PageParam [url=" + url + ", type=" + type + ", parentId="
				+ parentId + ", province=" + province + "]";
	}
}
