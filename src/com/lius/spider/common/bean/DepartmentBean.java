package com.lius.spider.common.bean;

import java.util.UUID;

public class DepartmentBean {
	private String id;
	
	private String hospitalId;
	
	private String name;
	
	private String telephone;
	
	public DepartmentBean() {
		
	}
	
	public DepartmentBean(String hospitalId) {
		this.id = UUID.randomUUID().toString();
		this.hospitalId = hospitalId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getHospitalId() {
		return hospitalId;
	}

	public void setHospitalId(String hospitalId) {
		this.hospitalId = hospitalId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	@Override
	public String toString() {
		return "DepartmentBean [id=" + id + ", hospitalId=" + hospitalId
				+ ", name=" + name + ", telephone=" + telephone + "]";
	}
}
