package com.lius.spider.common.bean;

import java.util.UUID;

public class AreaBean {
	
	private String id;
	
	private String province;
	
	private String city;
	
	public AreaBean() {
		
	}
	
	public AreaBean(String province, String city) {
		this.id = UUID.randomUUID().toString();
		this.province = province;
		this.city = city;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return "AreaBean [id=" + id + ", province=" + province + ", city="
				+ city + "]";
	}
}
