package com.lius.spider.dao;

import java.util.List;
import java.util.Map;

import com.lius.spider.common.bean.AreaBean;
import com.lius.spider.common.bean.DepartmentBean;
import com.lius.spider.common.bean.DoctorBean;
import com.lius.spider.common.bean.HospitalBean;

public interface DoctorDao {
	
	void insertArea(AreaBean area);
	
	void insertHospital(HospitalBean hospital);
	
	void insertDepartment(DepartmentBean department);
	
	void insertDoctor(DoctorBean doctor);
	
	List<Map<String, Object>> queryAllDoctors(int start, int limit);
	
	int queryAllDoctorsCount();
	
	int queryAreaCount(AreaBean area);
	
	int queryHospitalCount(HospitalBean hospital);
	
	int queryDepartmentCount(DepartmentBean department);
	
	void deleteDoctorsByDepartmentId(String departmentId);
	
	String queryAreaId(AreaBean area);
	
	String queryHospitalId(HospitalBean hospital);
	
	String queryDepartmentId(DepartmentBean department);

	List<Map<String, Object>> findDoctors(int start, int limit);

	long countDoctors();
	
	boolean insertCNKIUrl(String url);
}
