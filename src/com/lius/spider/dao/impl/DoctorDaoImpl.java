package com.lius.spider.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;

import com.lius.spider.common.bean.AreaBean;
import com.lius.spider.common.bean.DepartmentBean;
import com.lius.spider.common.bean.DoctorBean;
import com.lius.spider.common.bean.HospitalBean;
import com.lius.spider.common.utils.ColumnMapRowMapper;
import com.lius.spider.common.utils.SpiderUtils;
import com.lius.spider.dao.DoctorDao;

public class DoctorDaoImpl implements DoctorDao {

	protected JdbcTemplate jdbcTemplate;

	public DoctorDaoImpl(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public void insertArea(AreaBean area) {
		String sql = "insert  into `t_area`(`id`,`province`,`city`) values ('"
				+ area.getId() + "','" + area.getProvince() + "','"
				+ area.getCity() + "')";
		jdbcTemplate.execute(sql);

	}

	public void insertHospital(HospitalBean hospital) {
		String sql = "insert  into `t_hospital`(`id`,`areaId`,`name`,`grade`,`address`,`telephone`) values ('"
				+ hospital.getId()
				+ "','"
				+ hospital.getAreaId()
				+ "','"
				+ hospital.getName()
				+ "','"
				+ hospital.getGrade()
				+ "','"
				+ hospital.getAddress() + "','" + hospital.getTelephone() + "')";
		jdbcTemplate.execute(sql);
	}

	public void insertDepartment(DepartmentBean department) {
		String sql = "insert  into `t_department`(`id`,`hospitalId`,`name`,`telephone`) values ('"
				+ department.getId()
				+ "','"
				+ department.getHospitalId()
				+ "','"
				+ department.getName() + "','" + department.getTelephone() + "')";
		jdbcTemplate.execute(sql);

	}

	public void insertDoctor(DoctorBean doctor) {
		String sql = "insert  into `t_doctor`(`id`,`departmentId`,`name`,`photo`,`title`,`skills`,`experience`,`thanksLetters`," +
				"`gift`,`workingTimes`,`votes`,`canTelephone`,`canInternet`,`canAddNum`,`patientQuestionsNo`,`questionsReplyNo`,`addNumNo`,`url`) values ('"
				+ doctor.getId()
				+ "','"
				+ doctor.getDepartmentId()
				+ "','"
				+ doctor.getName()
				+ "','"
				+ doctor.getPhoto()
				+ "','"
				+ doctor.getTitle()
				+ "','"
				+ doctor.getSkills()
				+ "','"
				+ doctor.getExperience().replaceAll("'", "��")
				+ "','"
				+ doctor.getThanksLetters()
				+ "','"
				+ doctor.getGift()
				+ "','"
				+ SpiderUtils.convListToStr(doctor.getWorkingTimes())
				+ "','"
				+ doctor.getVotes()
				+ "','"
				+ doctor.getCanTelephone()
				+ "','"
				+ doctor.getCanInternet()
				+ "','" + doctor.getCanAddNum() 
				+ "','" + doctor.getPatientQuestionsNo()
				+ "','" + doctor.getQuestionsReplyNo()
				+ "','" + doctor.getAddNumNo()
				+ "','" + doctor.getUrl()
				+ "');";
		jdbcTemplate.execute(sql);
	}

	public List<Map<String, Object>> queryAllDoctors(int start, int limit) {
		String sql = "SELECT t4.province, t4.city, t3.name hospitalName, t3.address, t3.telephone hospitalTel, t2.name departmentName, t2.telephone departmentTel, t1.* FROM t_doctor t1 LEFT JOIN t_department t2 ON t1.departmentId = t2.id LEFT JOIN t_hospital t3 ON t2.hospitalId = t3.id LEFT JOIN t_area t4 ON t3.areaId = t4.id LIMIT " + start + "," + limit;
		return jdbcTemplate.query(sql.toString(), new ColumnMapRowMapper());
	}

	public int queryAllDoctorsCount() {
		String sql = "SELECT count(*) FROM t_doctor t1 LEFT JOIN t_department t2 ON t1.departmentId = t2.id LEFT JOIN t_hospital t3 ON t2.hospitalId = t3.id LEFT JOIN t_area t4 ON t3.areaId = t4.id";
		return jdbcTemplate.queryForInt(sql);
	}

	public int queryAreaCount(AreaBean area) {
		String sql = "SELECT COUNT(*) FROM t_area WHERE province = '" + area.getProvince() + "' AND city = '" + area.getCity() + "'";
		return jdbcTemplate.queryForInt(sql);
	}

	public int queryHospitalCount(HospitalBean hospital) {
		String sql = "SELECT COUNT(*) FROM t_hospital WHERE NAME = '" + hospital.getName() + "'";
		return jdbcTemplate.queryForInt(sql);
	}

	public int queryDepartmentCount(DepartmentBean department) {
		String sql = "SELECT COUNT(*) FROM t_department WHERE NAME = '" + department.getName() + "'";
		return jdbcTemplate.queryForInt(sql);
	}

	public void deleteDoctorsByDepartmentId(String departmentId) {
		String sql = "DELETE FROM t_doctor WHERE departmentId = '" + departmentId + "'";
		jdbcTemplate.execute(sql);
	}

	public String queryAreaId(AreaBean area) {
		String sql = "SELECT id FROM t_area WHERE province = '" + area.getProvince() + "' AND city = '" + area.getCity() + "'";
		return String.valueOf(jdbcTemplate.queryForMap(sql).get("id"));
	}

	public String queryHospitalId(HospitalBean hospital) {
		String sql = "SELECT id FROM t_hospital WHERE NAME = '" + hospital.getName() + "'";
		return String.valueOf(jdbcTemplate.queryForMap(sql).get("id"));
	}

	public String queryDepartmentId(DepartmentBean department) {
		String sql = "SELECT id FROM t_department WHERE NAME = '" + department.getName() + "'";
		return String.valueOf(jdbcTemplate.queryForMap(sql).get("id"));
	}

	public List<Map<String, Object>> findDoctors(int start, int limit) {
		String sql = "SELECT d.PHY_ID, d.PHY_NAME FROM HAODF_DOCTOR d LIMIT " + start + "," + limit;
		return jdbcTemplate.query(sql.toString(), new ColumnMapRowMapper());
	}

	public long countDoctors() {
		String sql = "SELECT COUNT(*) as total FROM HAODF_DOCTOR";
		return (Long) jdbcTemplate.queryForMap(sql).get("total");
	}

	@Override
	public boolean insertCNKIUrl(String url) {
		String sql = "insert  into `CNKI_URL`(`URL`) values ('" + url + "')";
		try {
			jdbcTemplate.execute(sql);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("重复了...");
			return false;
		}
		return true;
	}
}
