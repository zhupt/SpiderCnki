package com.lius.spider.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.WebElement;
import org.springframework.jdbc.core.JdbcTemplate;

import com.lius.spider.cnki.CNKIMain;
import com.lius.spider.dao.DoctorDao;
import com.lius.spider.dao.impl.DoctorDaoImpl;

/**
 * 
* 功能描述: 文件工具类
* 作者： 朱朋涛
* 日期： 2017年2月7日 下午3:15:38 
* 版本：V1.0
 */
public final class FileUtils {

	/**
	 * 
	
	* @Description: 保存url地址到文本
	
	* @param @param fileName
	* @param @param elements
	* @param @throws IOException    设定文件 
	
	* @return void    返回类型 
	
	* @throws
	 */
	public static void saveURLToFile(String fileName, List<WebElement> elements) {
/*		BufferedWriter writer = null;
		try {
			String path = CNKIMain.FILE_PATH + "/cnkiUrl_" + fileName + ".txt";
			File file = new File(CNKIMain.FILE_PATH);
			if(!file.exists())file.mkdirs();
			writer  = new BufferedWriter(new FileWriter(new File(path), true));
			for(WebElement element : elements){
				writer.write(element.getAttribute("href"));
				writer.write("\r\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				writer.flush();
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}*/
		DoctorDao doctorDao = new DoctorDaoImpl(CNKIMain.template);
		for(WebElement element : elements){
			try {
				doctorDao.insertCNKIUrl(element.getAttribute("href"));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}
	
	/**
	 * 
	
	* @Description: 从字符串中提取数字
	
	* @param @param content
	* @param @return    设定文件 
	
	* @return String    返回类型 
	
	* @throws
	 */
	public static String getNumbers(String content) {  
	    Pattern pattern = Pattern.compile("\\d+");  
	    Matcher matcher = pattern.matcher(content);  
	    while (matcher.find()) {  
	       return matcher.group(0);  
	    }  
	    return "";  
	}

	/**
	 * 
	
	* @Description: 把字符串信息写到文本中
	
	* @param @param countPage    设定文件 
	
	* @return void    返回类型 
	
	* @throws
	 */
	public static void toFile(String countPage) {
		BufferedWriter writer = null;
		try {
			String path = CNKIMain.FILE_PATH + "/countpage.txt";
			File file = new File(CNKIMain.FILE_PATH);
			if(!file.exists())file.mkdirs();
			writer  = new BufferedWriter(new FileWriter(new File(path), true));
			writer.write(countPage);
			writer.write("\r\n");
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				writer.flush();
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}		
	}

}
