package com.lius.spider.utils;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * 
* 功能描述: url检索工具
* 作者： 朱朋涛
* 日期： 2017年2月7日 下午2:59:20 
* 版本：V1.0
 */
public final class SearchUtils {

	/**
	 * 
	
	* @Description: 搜索cnki文章具体地址
	
	* @param @param params
	* @param @throws InterruptedException    设定文件 
	
	* @return void    返回类型 
	
	* @throws
	 */
	public static void searchURL(Map<String, String> params, WebDriver DRIVER) throws InterruptedException {
        String countPage = null;
        try {
            DRIVER.get(params.get("url"));
            DRIVER.findElement(By.id("advacneId")).click();
            DRIVER.findElement(By.id("txt_1_value1")).sendKeys(params.get("diseaseKind"));
            DRIVER.findElement(By.id("au_1_value1")).sendKeys(params.get("author"));
            WebElement btn = DRIVER.findElement(By.id("btnSearch"));
            Thread.sleep(5000);
            btn.click();
            Thread.sleep(5000);
            DRIVER.switchTo().frame("iframeResult");
        	WebElement wcountPage = DRIVER.findElement(By.cssSelector(".pageBar_min .pagerTitleCell"));
        	countPage = wcountPage.getText();
        } catch (Exception e) {
			e.printStackTrace();
		}
        if(countPage != null){
        	FileUtils.toFile(countPage);
        	long counts = 0;
    		counts = Long.parseLong(FileUtils.getNumbers(countPage));
	    	if(counts > 0){
	        	if(counts / 20 > 15){
	        		DRIVER.findElement(By.cssSelector("a[href$='recordsperpage=50']")).click();
	        	}
	            do {
	            	List<WebElement> elements = DRIVER.findElements(By.className("fz14"));
	            	FileUtils.saveURLToFile(params.get("start")+"_" + params.get("limit"), elements);
	            	WebElement wclick = null;
	        		try {
	        			wclick = DRIVER.findElement(By.id("Page_next"));
	    			} catch (Exception e) {
	    				e.printStackTrace();
	    				break;
	    			}
	        		if(wclick != null)wclick.click();
	            } while (true);
	        }
        }
	}
}
