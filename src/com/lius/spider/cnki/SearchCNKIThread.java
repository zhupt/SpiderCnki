package com.lius.spider.cnki;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

import com.lius.spider.utils.SearchUtils;

/**
 * 
* 功能描述: 检索cnki文献地址线程类
* 作者： 朱朋涛
* 日期： 2017年2月7日 下午3:08:26 
* 版本：V1.0
 */
public class SearchCNKIThread extends Thread{

	private Map<String, Integer> params = null;
	private List<Map<String, Object>> doctors = null;
	
	public SearchCNKIThread(Map<String, Integer> params, List<Map<String, Object>> doctors) {
		super();
		this.params = params;
		this.doctors = doctors;
	}

	@Override
	public void run() {
		File pathToBinary = new File(CNKIMain.FIREFOX);
        FirefoxBinary ffBinary = new FirefoxBinary(pathToBinary);
        FirefoxProfile firefoxProfile = new FirefoxProfile();
        firefoxProfile.setPreference("permissions.default.stylesheet", 2);
        firefoxProfile.setPreference("dom.ipc.plugins.enabled.libflashplayer.so",false);
        WebDriver DRIVER = new FirefoxDriver(ffBinary, firefoxProfile); 
        DRIVER.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);  
		if(doctors != null && doctors.size() > 0){
	        try {
				for(Map<String, Object> doctor : doctors){
					Map<String, String> map = new HashMap<String, String>();
					map.put("url", "http://www.cnki.net/");
					map.put("diseaseKind", "肺癌");
					map.put("author", doctor.get("PHY_NAME").toString());
					map.put("start", params.get("start").toString());
					map.put("limit", params.get("limit").toString());
					SearchUtils.searchURL(map, DRIVER);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		DRIVER.close();
		DRIVER.quit();
	}

}
