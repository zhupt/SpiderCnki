package com.lius.spider.cnki;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

import com.lius.spider.dao.DoctorDao;
import com.lius.spider.dao.impl.DoctorDaoImpl;
/**
 * 
* 功能描述: 检索cnki文献地址页面url
* 作者： 朱朋涛
* 日期： 2017年2月7日 下午3:07:14 
* 版本：V1.0
 */
public class CNKIMain {
	
	public static JdbcTemplate template = null;
	public static String FIREFOX, FILE_PATH;
	private static String start;
	private static int limit;
	static {
		try {
			Properties props = new Properties();
			props.load(CNKIMain.class.getClassLoader().getResourceAsStream("cnki.properties"));
			FIREFOX = props.getProperty("cnki.browser");
			FILE_PATH = props.getProperty("cnki.file");
			start = props.getProperty("cnki.doctor.start");
			limit = Integer.parseInt(props.getProperty("cnki.doctor.limit"));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
	}

	public static void main(String[] args) throws InterruptedException {
		ApplicationContext app = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
		template = (JdbcTemplate) app.getBean("jdbcTemplate");
		DoctorDao doctorDao = new DoctorDaoImpl(template);
		String[] array = start.split(",");
		for(String as : array){
			int index = (Integer.parseInt(as) - 1) * 1000 + 1;
			List<Map<String, Object>> doctors = doctorDao.findDoctors(index, limit);
			Map<String, Integer> params = new HashMap<String, Integer>();
			params.put("start", index);
			params.put("limit", index + limit - 1);
			new SearchCNKIThread(params, doctors).start();
		}
	}
}
